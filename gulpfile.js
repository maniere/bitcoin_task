const gulp = require('gulp');
const stylus = require('gulp-stylus');
const gulpPlumber = require('gulp-plumber');
const gulpSourcemaps = require('gulp-sourcemaps');
const gulpNotify = require('gulp-notify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create()
const prefixer = require('gulp-autoprefixer')
const del = require('del');
const concat = require('gulp-concat')

const path ={
	"styles" :
		{
		"src" :  "./assets/_main.styl",
		"watch":  "./**/*.styl",
		"dest" : "./build/"
		},
	"images" :
		{
		"src" :   "./assets/i/*.{jpg,JPG,JPEG,jpeg,png,PNG,SVG,svg,gif,GIF}",
		"dest" : "./build/i/"
		},
	"fonts" :
		{
		"src" :   "./assets/f/**/*.*",
		"dest" :   "./build/f/"
		},
	"all" : "./build/"
} 


gulp.task('fonts', function(){
	return gulp.src(path.fonts.src)
		.pipe(gulp.dest(path.fonts.dest))
});

gulp.task('images', function(){
	return gulp.src(path.images.src)
		.pipe(imagemin())
		.pipe(gulp.dest(path.images.dest))
});

gulp.task('clean', function(){
	return del(path.all)
});


gulp.task('styles', function(){
	return gulp.src(path.styles.src)
		.pipe(gulpPlumber({'errorHandler' : gulpNotify.onError("Error: <%= error %>")}))
		.pipe(gulpSourcemaps.init({includeContent: true}))
		.pipe(stylus({'include css': true}))
		.pipe(prefixer())
		.pipe(gulpSourcemaps.write())
		.pipe(concat('main.css'))
		.pipe(gulp.dest(path.all))
	});

gulp.task('serve', function(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
	browserSync.watch(['./build/**/*.*', './index.html', './s.js']).on('change', browserSync.reload);
	});


gulp.task('watch', function() {
  gulp.watch(path.styles.watch, gulp.series('styles'));
});


gulp.task('build', gulp.series('clean', 'styles', 'images', 'fonts'));

gulp.task('dev', gulp.series('clean', 'styles', 'images', 'fonts', gulp.parallel('watch', 'serve')));

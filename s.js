(function(){
	'use strict';

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	var BitcoinLanding = (function() {
		function BitcoinLanding() {
			this.fields = {};
			this.cacheVars();
			this.eventHandler();
		}


		BitcoinLanding.prototype.cacheVars = function() {
			this.$form = $('.form__fields');
			this.$message = this.$form.find('.message');
			this.fields.$firstName = this.$form.find('[data-var="fname"]');
			this.fields.$lastName = this.$form.find('[data-var="lname"]');
			this.fields.$country = this.$form.find('[data-var="country"]');
			this.fields.$code = this.$form.find('[data-var="code"]');
			this.fields.$phone = this.$form.find('[data-var="phone"]');
			this.fields.$email = this.$form.find('[data-var="email"]');
			this.fields.$submit = this.$form.find('.form__submit');
		};

		BitcoinLanding.prototype.eventHandler = function() {
			var that = this;
			for (let i in this.fields) {

				this.fields[i].on('focus', function() {
					that.fields[i].hasError = 0;
					$(this).on('keyup', function(event){
						if (event.which == 13) {
							that.formSubmitTrigger();
						}
					});
					$(this).removeClass('form__fields--invalid');
					if ($(this).prev().hasClass('validation-error')) {
						var $errorMes = $(this).prev(); 
						$errorMes.hide(300)
						setTimeout(function(){
							$errorMes.remove()
						}, 400)

					}
				});
				
			}

			this.fields.$submit.on('click', this.formSubmitTrigger.bind(this));
		};

		BitcoinLanding.prototype.formSubmitTrigger = function() {
			if (!this.validate()) {
				this.wrongValidateHandler();
			}
			else {
				this.sendRequest();
			}
		};

		BitcoinLanding.prototype.validate = function() {
			var noErrors = 1;
			if ((this.fields.$firstName.val().trim().length > 50) || (this.fields.$firstName.val().trim().length < 2 )) {
					noErrors = 0;
					this.fields.$firstName.hasError = 1;
					this.fields.$firstName.minLength = 1;
					this.fields.$firstName.maxLength = 50;
			}
			if ((this.fields.$lastName.val().trim().length > 50) || (this.fields.$lastName.val().trim().length < 2)) {
					noErrors = 0;
					this.fields.$lastName.hasError = 1;
					this.fields.$lastName.minLength = 1;
					this.fields.$lastName.maxLength = 50;
			}
			if ((this.fields.$country.val().trim().length > 50) || (this.fields.$country.val().trim().length < 2)) {
					noErrors = 0;
					this.fields.$country.hasError = 1;
					this.fields.$country.minLength = 1;
					this.fields.$country.maxLength = 50;
			}
			if ((this.fields.$code.val().trim().length > 3) || (this.fields.$code.val().trim().length === 0)) {
					noErrors = 0;
					this.fields.$code.hasError = 1;
					this.fields.$code.minLength = 0;
					this.fields.$code.maxLength = 3;
			}
			if ((this.fields.$phone.val().trim().length > 7) || (this.fields.$phone.val().trim().length < 4)) {
					noErrors = 0;
					this.fields.$phone.hasError = 1;
					this.fields.$phone.minLength = 3;
					this.fields.$phone.maxLength = 7;
			}
			var email = this.fields.$email.val().trim();
			if ((email.length > 50) || (email.length < 5) || (!validateEmail(email))) {
					noErrors = 0;
					this.fields.$email.hasError = 1;
					this.fields.$email.isEmail = 1;

					// this.fields.$email.minLength = 2;
					// this.fields.$email.maxLength = 50;
			}
			
			
			return noErrors;
		};

		BitcoinLanding.prototype.showValidateMessage = function(params) {
			var message;
			var $field = $(params.field)
			if (params.email) {
				message = 'Email is not valid!'
			}
			else {
				message = 'Value length must be bigger then ' + params.minLength + ' and smaller then ' + params.maxLength; 
			}
			$field.before('<div class="validation-error">' + message + '</div>');
			console.log(message)
		}

		BitcoinLanding.prototype.wrongValidateHandler = function() {
			for (let i in this.fields) {
				if ((this.fields[i].hasError) && (!this.fields[i].hasClass('form__fields--invalid'))) {
					this.fields[i].addClass('form__fields--invalid');
					if (this.fields[i].isEmail) {
						this.showValidateMessage({field: this.fields[i], email: true})
					} else {
						this.showValidateMessage({field: this.fields[i], minLength: this.fields[i].minLength, maxLength: this.fields[i].maxLength})
					}
				} 
			}
		};

		BitcoinLanding.prototype.sendRequest = function() {
			var that = this;
			$.ajax({
				type: 'POST',
				url: 'server/requestHandler.php',
				data: {
					firstName : this.fields.$firstName.val().trim(),
					lastName : this.fields.$lastName.val().trim(),
					country : this.fields.$country.val().trim(),
					code : this.fields.$code.val().trim(),
					phone : this.fields.$phone.val().trim(),
					email : this.fields.$email.val().trim()
				},
				success: function(e) {
					var answer = JSON.parse(e);
					if (answer.status == 200) {
						that.returnSuccess();
					}
					else {
						that.returnError(answer.message);
					}
					that.endSession();
				}
			})
		};

		BitcoinLanding.prototype.returnSuccess = function() {
			this.$message.html('Successfull registration!');
			this.$message.addClass('message--visible');
			this.$message.addClass('message--success');
		};

		BitcoinLanding.prototype.returnError = function(message) {
			this.$message.html(message);
			this.$message.addClass('message--visible');
			this.$message.addClass('message--error');
		};

		BitcoinLanding.prototype.endSession = function() {
			var that = this;
			setTimeout(function(){
				$(that.$message).html('');
				$(that.$message).removeClass('message--visible');
				$(that.$message).removeClass('message--error');
				$(that.$message).removeClass('message--success');
			}, 4000);
		};

		return BitcoinLanding;

	})();

	$(document).ready(function(){
		var BL = new BitcoinLanding();

	});
})();
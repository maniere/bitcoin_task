<?php 

	//no database, no need to protect it :-)
	$firstName = htmlspecialchars($_POST['firstName']);
	$lastName = htmlspecialchars($_POST['lastName']);
	$country = htmlspecialchars($_POST['country']);
	$code = htmlspecialchars($_POST['code']);
	$phone = htmlspecialchars($_POST['phone']);
	$email = htmlspecialchars($_POST['email']);

	$emails = array(
		'test0@gmail.com',
		'test1@gmail.com',
		'test2@gmail.com',
		'test3@gmail.com'
		);

	if (!in_array($email, $emails)) {
		echo json_encode(array(
			'status' => 200,
			'message' => 'OK'
			));	
	}
	else {
		echo json_encode(array(
			'status' => 500,
			'message' => 'Email was already registered'
			));
	}
	


?>